# Bioinformatics cookiecutter project template

## Usage

### Prepare data structure and symlink data

```
make data
```

### Get list of tools and versions automatically to your project report

```
make tools
```
