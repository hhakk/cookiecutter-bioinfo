#!/usr/bin/env sh

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
#SCRIPTPATH=$(dirname "$SCRIPT")
SCRIPTPATH="."

TOOLS_FILE="$SCRIPTPATH/TOOLS.csv"

REPORT_FILE="$SCRIPTPATH/reports/report.Rmd"

for line in $(cat $TOOLS_FILE); do
	name_pretty=$(echo $line|cut -f1 -d";")
	tool_path=$(echo $line|cut -f2 -d";")
	tool_version=$($tool_path --version)
	to_append="
**$name_pretty**

:    $tool_version"
	updated_report=$(sed "/# Tools/a$(printf %q "$to_append")" $REPORT_FILE|sed "s/^\$'$//g")
	echo "$updated_report" > $REPORT_FILE
done
