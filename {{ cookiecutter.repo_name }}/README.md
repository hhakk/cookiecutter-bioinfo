{{cookiecutter.project_name}}
==============================

{{cookiecutter.description}}

Project Organization
------------

    ├── Makefile           <- Makefile with commands like `make data` or `make tools`
    ├── README.md          <- The top-level README for developers using this project.
    ├── TOOLS.tsv          <- A semicolon-separated file of your tools with the format `Pretty Name;/path/to/executable`
    ├── data
    │   └── raw            <- The original, immutable data dump.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Project report
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── src                <- Source code for use in this project, store all your scripts here


--------

## Usage

* To symlink the data files to your project, simply run `make data`.

* To add tool versions to your project report, fill out `TOOLS.csv` and run `make tools`.

* To compile your RMarkdown report, run `make report`.
